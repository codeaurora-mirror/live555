/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2017 Live Networks, Inc.  All rights reserved.
// A simplified version of "H264or5VideoStreamFramer" that takes only complete,
// discrete frames (rather than an arbitrary byte stream) as input.
// This avoids the parsing and data copying overhead of the full
// "H264or5VideoStreamFramer".
// Implementation
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "H264or5VideoStreamDiscreteFramer.hh"

H264or5VideoStreamDiscreteFramer
::H264or5VideoStreamDiscreteFramer(int hNumber, UsageEnvironment& env, FramedSource* inputSource)
  : H264or5VideoStreamFramer(hNumber, env, inputSource, False/*don't create a parser*/, False) {
}

H264or5VideoStreamDiscreteFramer::~H264or5VideoStreamDiscreteFramer() {
}

void H264or5VideoStreamDiscreteFramer::doGetNextFrame() {
  // Arrange to read data (which should be a complete H.264 or H.265 NAL unit)
  // from our data source, directly into the client's input buffer.
  // After reading this, we'll do some parsing on the frame.
  fInputSource->getNextFrame(fTo, fMaxSize,
                             afterGettingFrame, this,
                             FramedSource::handleClosure, this);
}

void H264or5VideoStreamDiscreteFramer
::afterGettingFrame(void* clientData, unsigned frameSize,
                    unsigned numTruncatedBytes,
                    struct timeval presentationTime,
                    unsigned durationInMicroseconds) {
  H264or5VideoStreamDiscreteFramer* source = (H264or5VideoStreamDiscreteFramer*)clientData;
  source->afterGettingFrame1(frameSize, numTruncatedBytes, presentationTime, durationInMicroseconds);
}

void H264or5VideoStreamDiscreteFramer
::afterGettingFrame1(unsigned frameSize, unsigned numTruncatedBytes,
                     struct timeval presentationTime,
                     unsigned durationInMicroseconds) {
  // Get the "nal_unit_type", to see if this NAL unit is one that we want to save a copy of:
  u_int8_t nal_unit_type;
  u_int8_t* nalStart;
  unsigned nalSize;
  unsigned char* _data = fTo;
  unsigned _frameSize = frameSize;
  while (!getNextNALUnit(&_data, &_frameSize, &nalStart, &nalSize)) {
      if (fHNumber == 264 && nalSize >= 1) {
          nal_unit_type = nalStart[0]&0x1F;
      } else if (fHNumber == 265 && nalSize >= 2) {
          nal_unit_type = (nalStart[0]&0x7E)>>1;
      } else {
        // This is too short to be a valid NAL unit, so just assume a bogus nal_unit_type
        nal_unit_type = 0xFF;
      }

      if (isVPS(nal_unit_type)) { // Video parameter set (VPS)
          saveCopyOfVPS(nalStart, nalSize);
      } else if (isSPS(nal_unit_type)) { // Sequence parameter set (SPS)
          saveCopyOfSPS(nalStart, nalSize);
      } else if (isPPS(nal_unit_type)) { // Picture parameter set (PPS)
          saveCopyOfPPS(nalStart, nalSize);
      } else if (nalUnitEndsAccessUnit(nal_unit_type)) {
          fPictureEndMarker = True;
      }
  }

  if (frameSize >= 4 && !memcmp("\x00\x00\x00\x01", fTo, 4)) {
    frameSize -= 4;
    memmove(fTo, fTo + 4, frameSize);
  }
  // Finally, complete delivery to the client:
  fFrameSize = frameSize;
  fNumTruncatedBytes = numTruncatedBytes;
  fPresentationTime = presentationTime;
  fDurationInMicroseconds = durationInMicroseconds;
  afterGetting(this);
}

Boolean H264or5VideoStreamDiscreteFramer::nalUnitEndsAccessUnit(u_int8_t nal_unit_type) {
  // Check whether this NAL unit ends the current 'access unit' (basically, a video frame).
  //  Unfortunately, we can't do this reliably, because we don't yet know anything about the
  // *next* NAL unit that we'll see.  So, we guess this as best as we can, by assuming that
  // if this NAL unit is a VCL NAL unit, then it ends the current 'access unit'.
  //
  // This will be wrong if you are streaming multiple 'slices' per picture.  In that case,
  // you can define a subclass that reimplements this virtual function to do the right thing.

  return isVCL(nal_unit_type);
}

int H264or5VideoStreamDiscreteFramer
::getNextNALUnit(u_int8_t **_data, unsigned *_frameSize,
                 u_int8_t **nalStart, unsigned *nalSize) {
    u_int8_t *data = *_data;
    unsigned size = *_frameSize;

    *nalStart = NULL;
    *nalSize = 0;

    if (size < 3) {
        return -EAGAIN;
    }

    unsigned offset = 0;

    if (data[offset] == 0x00) {
        for (; offset + 2 < size; ++offset) {
            if (data[offset + 2] == 0x01 && data[offset] == 0x00
                    && data[offset + 1] == 0x00) {
                break;
            }
        }
        if (offset + 2 >= size) {
            *_data = &data[offset];
            *_frameSize = 2;
            return -EAGAIN;
        }
        offset += 3;
    }

    size_t startOffset = offset;

    for (;;) {
        while (offset < size && data[offset] != 0x01) {
            ++offset;
        }

        if (offset == size) {
            *nalStart = &data[startOffset];
            *nalSize = offset - startOffset;
            *_data = NULL;
            *_frameSize = 0;
            return 0;
        }

        if (data[offset - 1] == 0x00 && data[offset - 2] == 0x00) {
            break;
        }

        ++offset;
    }

    unsigned endOffset = offset -2;
    while (endOffset > startOffset + 1 && data[endOffset - 1] == 0x00) {
        --endOffset;
    }

    *nalStart = &data[startOffset];
    *nalSize = endOffset - startOffset;

    if (offset + 2 < size) {
        *_data = &data[offset - 2];
        *_frameSize = size - offset + 2;
    } else {
        *_data = NULL;
        *_frameSize = 0;
    }

    return 0;
}
